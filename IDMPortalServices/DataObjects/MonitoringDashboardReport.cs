﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Philips.HealthCare.IDM.Portal.DataObjects {
    /// <summary>
    /// MonitoringDashboardReport
    /// </summary>
    public class MonitoringDashboardReport {
        /// <summary>
        /// SiteKey 
        /// </summary>
        public string SiteKey { get; set; }
        /// <summary>
        /// Version 
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// SiteName 
        /// </summary>
        public string SiteName { get; set; }
        /// <summary>
        /// Service 
        /// </summary>
        public string Service { get; set; }
        /// <summary>
        /// Status 
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// isNoteAvail 
        /// </summary>
        public bool isNoteAvail { get; set; }
        /// <summary>
        /// OneEMS
        /// </summary>
        public bool OneEMS { get; set; }
        /// <summary>
        /// HostIP 
        /// </summary>
        public string HostIP { get; set; }
        /// <summary>
        /// HostName 
        /// </summary>
        public string HostName { get; set; }
        /// <summary>
        /// Duration 
        /// </summary>
        public string Duration { get; set; }
        /// <summary>
        /// Description 
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// LastUpdated 
        /// </summary>
        public string LastUpdated { get; set; }
    }
}