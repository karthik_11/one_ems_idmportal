﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips.HealthCare.IDM.Portal.DataObjects {
    /// <summary>
    /// Message Class
    /// </summary>
    public class Message {
        /// <summary>
        /// _id
        /// </summary>
        public string elementid { get; set; }
        /// <summary>
        /// casenum
        /// </summary>
        public string caseid { get; set; }
        /// <summary>
        /// casetype
        /// </summary>
        public string casetype { get; set; }
        /// <summary>
        /// sitekey
        /// </summary>
        public string sitekey { get; set; }
        /// <summary>
        /// version
        /// </summary>
        public string version { get; set; }
        /// <summary>
        /// sitename
        /// </summary>
        public string sitename { get; set; }
        /// <summary>
        /// service
        /// </summary>
        public string servicenamespace { get; set; }
        /// <summary>
        /// eventfilter
        /// </summary>
        public string eventfilter { get; set; }
        /// <summary>
        /// servicedisplayname
        /// </summary>
        public string namespaceabbreviation { get; set; }
        /// <summary>
        /// status
        /// </summary>
        public string status { get; set; }
        /// <summary>
        /// hostipaddress
        /// </summary>
        public string hostipaddress { get; set; }
        /// <summary>
        /// hostname
        /// </summary>
        public string hostname { get; set; }
        /// <summary>
        /// duration
        /// </summary>
        public string duration { get; set; }
        /// <summary>
        /// durationdt
        /// </summary>
        public string durationdt { get; set; }
        /// <summary>
        /// description
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// type
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// lastreceived
        /// </summary>
        public string lastreceived { get; set; }     
    }
}
