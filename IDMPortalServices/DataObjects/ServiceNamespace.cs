﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips.HealthCare.IDM.Portal.DataObjects {
    /// <summary>
    /// SiteInfo
    /// </summary>
    public class ServiceNamespace {
        /// <summary>
        /// _id
        /// </summary>
        public string _id { get; set; }
        /// <summary>
        /// EventFilter
        /// </summary>
        public string eventfilter { get; set; }
        /// <summary>
        /// Root
        /// </summary>
        public string root { get; set; }
        /// <summary>
        /// NamespaceAbbreaviation
        /// </summary>
        public string namespaceabbreviation { get; set; }
        /// <summary>
        /// Namespace
        /// </summary>
        public string servicenamespace { get; set; }
        /// <summary>
        /// DisplayName
        /// </summary>
        public string displayname { get; set; }
    }
}