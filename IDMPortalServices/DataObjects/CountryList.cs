﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips.HealthCare.IDM.Portal.DataObjects
{
    public class Country
    {
        public string market { get; set; }
        public string sap_code { get; set; }
        public string country { get; set; }
    }
}
