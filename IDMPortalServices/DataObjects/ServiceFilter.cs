﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips.HealthCare.IDM.Portal.DataObjects {
    public class ServiceFilter {
        public string servicedisplayname { get; set; }
    }

    public class Status {
        public string status { get; set; }
    }

    public class SytemFilter {
        public IList<SiteInfo> sites { get; set; }
        public IList<ServiceFilter> servicefilters { get; set; }
        public IList<Status> status { get; set; }

        public SytemFilter() {
            sites = new List<SiteInfo>();
            servicefilters = new List<ServiceFilter>();
            status = new List<Status>();
        }
    }
}
