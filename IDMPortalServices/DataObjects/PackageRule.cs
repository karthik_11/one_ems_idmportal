﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Philips.HealthCare.IDM.Portal.DataObjects
{
    /// <summary>
    /// PackageRule
    /// </summary>
    public class PackageRule
    {
        //public string _id { get; set; }
        /// <summary>
        /// package
        /// </summary>
        public string package { get; set; }
        /// <summary>
        /// rules
        /// </summary>
        public List<string> rules { get; set; }
        /// <summary>
        /// countries
        /// </summary>
        public List<string> countries { get; set; }
    }
}
