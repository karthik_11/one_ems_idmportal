﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;


namespace Philips.HealthCare.IDM.Portal.DataObjects {

    /// <summary>
    /// NotificationMessage
    /// </summary>
    [DataContract]    
    public class NotificationMessage {

        /// <summary>
        /// Constructor
        /// </summary>
        public NotificationMessage() {
        }

        /// <summary>
        /// caseid
        /// </summary>
        [DataMember]
        public string caseid { get; set; }
        /// <summary>
        /// elementid
        /// </summary>
        [DataMember]
        public object elementid { get; set; }
        /// <summary>
        /// hostaddress
        /// </summary>
        [DataMember]
        public string hostaddress { get; set; }
        /// <summary>
        /// hostname
        /// </summary>
        [DataMember]
        public string hostname { get; set; }
        /// <summary>
        /// service
        /// </summary>
        [DataMember]
        public string service { get; set; }
        /// <summary>
        /// type
        /// </summary>
        [DataMember]
        public string type { get; set; }
        /// <summary>
        /// status
        /// </summary>
        [DataMember]
        public string status { get; set; }
        /// <summary>
        /// siteid
        /// </summary>
        [DataMember]
        public string siteid { get; set; }
        /// <summary>
        /// version
        /// </summary>
        [DataMember]
        public string version { get; set; }
        /// <summary>
        /// sitename
        /// </summary>
        [DataMember]
        public string sitename { get; set; }
        /// <summary>
        /// output
        /// </summary>
        [DataMember]
        public string output { get; set; }
        /// <summary>
        /// state
        /// </summary>
        [DataMember]
        public string state { get; set; }
        /// <summary>
        /// timestamp
        /// </summary>
        [DataMember]
        [BsonDateTimeOptions(Representation = BsonType.String, Kind = DateTimeKind.Utc)]
        public DateTime timestamp { get; set; }
        /// <summary>
        /// perfdata
        /// </summary>
        [DataMember]
        public List<Perfdata> perfdata { get; set; }        
    }
}