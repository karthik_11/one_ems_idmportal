﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips.HealthCare.IDM.Portal.DataObjects
{
    public class ResponseObject
    {
        public int status { get; set; }
        public string message { get; set; }
        public string payload { get; set; }
    }

    public class JsonStringArray
    {
        public List<string> array { get; set; }
    }
}
