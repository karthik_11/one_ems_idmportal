﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.Logger;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class ProductController : ApiController
    {
        public HttpResponseMessage Get()
        {
            LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method of ProductController " });
            //var json = JArray.Parse("[{ 'name':'Anywhere Viewer', 'type':'Type1', 'version':'2.0', 'location':'Path1'},{'name':'Anywhere Viewer', 'type':'Type1', 'version':'2.0', 'location':'Path1'},{'name':'Anywhere Viewer', 'type':'Type1', 'version':'2.0', 'location':'Path1'}]");

            var json = "{'subscriptions': ['product1', 'product2', 'product3', 'product4', 'product5', 'product6', 'product7', 'product77', 'productX']}";

            return new HttpResponseMessage()
            {
                Content = new JsonContent(JObject.Parse(json))
            };
        }
    }
}