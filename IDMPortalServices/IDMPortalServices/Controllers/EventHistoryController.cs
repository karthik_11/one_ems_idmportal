﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.Logger;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Newtonsoft.Json;
using MongoDB.Driver;
using System.Net.Http.Formatting;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers {
    public class EventHistoryController : ApiController {
         MongoDbHelper helper;        

        /// <summary>
         /// Initializes a new instance of the <see cref="HostEventsController"/> class.
        /// </summary>
         public EventHistoryController() {
            helper = new MongoDbHelper();
        }

         public HttpResponseMessage Post([FromBody] PostJSONPayload payload)
         {
             JsonMediaTypeFormatter objFormatter = new JsonMediaTypeFormatter();
             string criteria = payload.payload;
             var settings = new JsonSerializerSettings() { DateFormatHandling = DateFormatHandling.MicrosoftDateFormat };
             var filter = JsonConvert.DeserializeObject<SearchCriteria>(criteria, settings);             
             LogManager.LogWriter.WriteLog(new LogMessage() { Description = "Post method of HostEventsController" });             
             MongoCursor<NotificationMessage> result = helper.GetEventHistory(filter);
             var httpResponseMessage = new HttpResponseMessage() {
                 Content = new ObjectContent(result.GetType(), result, objFormatter) 
             };
             return httpResponseMessage;
         }
    }
}