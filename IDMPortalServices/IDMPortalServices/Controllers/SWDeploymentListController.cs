﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.Logger;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using Newtonsoft.Json;
using Philips.HealthCare.IDM.Portal.IDM_ESBInterface;
using MongoDB.Bson;
using System.Net.Http.Formatting;
using MongoDB.Driver;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class SWDeploymentListController : ApiController
    {
        SWDeploymentHelper helper;        

        /// <summary>
        /// Initializes a new instance of the <see cref="SWDRequestController"/> class.
        /// </summary>
        public SWDeploymentListController()
        {
            helper = new SWDeploymentHelper();
        }

        public HttpResponseMessage Post([FromBody] PostJSONPayload payload)
        {
            JsonMediaTypeFormatter objFormatter = new JsonMediaTypeFormatter();
            string criteria = payload.payload;
            var settings = new JsonSerializerSettings() { DateFormatHandling = DateFormatHandling.MicrosoftDateFormat };
            var filter = JsonConvert.DeserializeObject<SearchCriteria>(criteria, settings);
            LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method of SWDeploymentListController" });
            MongoCursor<SWDeployment> result = helper.GetAllSWDeploymentRequests(filter);
            var httpResponseMessage = new HttpResponseMessage()
            {
                Content = new ObjectContent(result.GetType(), result, objFormatter)
            };
            return httpResponseMessage;
        }

        public HttpResponseMessage Get()
        {
            JsonMediaTypeFormatter objFormatter = new JsonMediaTypeFormatter();
            SearchCriteria criteria = new SearchCriteria();
            LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method of SWDeploymentListController" });
            MongoCursor<SWDeployment> result = helper.GetAllSWDeploymentRequests(criteria);
            var httpResponseMessage = new HttpResponseMessage()
            {
                Content = new ObjectContent(result.GetType(), result, objFormatter)
            };
            return httpResponseMessage;
        }      

    }
}