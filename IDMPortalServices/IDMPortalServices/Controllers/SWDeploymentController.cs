﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MongoDB.Bson;
using System.IO;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Net;
using MongoDB.Driver;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.Logger;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    /// <summary>
    /// Software Deployment Management Controller for Populating Packages and Hosts
    /// </summary>
    public class SWDeploymentController : ApiController
    {        
         /// <summary>
        /// Gets all the packages from the IDM Ecosystem for a Selected Site Id
        /// </summary>
        /// <param name="sitekey"></param>
        /// <returns>HttpResponseMessage</returns>
        [AcceptVerbs("GET")]
        [ActionName("Packages")]
        public HttpResponseMessage Packages(string sitekey)
        {
            HttpResponseMessage response;
            try
            {
                var result = string.Empty;
                JsonMediaTypeFormatter objFormatter = new JsonMediaTypeFormatter();
                response = EcosystemAPIHelper.GetEcosystemDeploymentPackages(sitekey);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result = response.Content.ReadAsStringAsync().Result;
                    response.Content = new ObjectContent(result.GetType(), result, objFormatter);

                }
            }
            catch (Exception ex)
            {
                response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(ex.Message)
                };
            }
            return response;
        }

        /// <summary>
        /// Gets all the Hosts from the IDM Ecosystem for a Selected Site Id
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns>HttpResponseMessage</returns>
        [AcceptVerbs("GET")]
        [ActionName("Hosts")]
        public HttpResponseMessage Hosts(string siteId)
        {
            HttpResponseMessage response;
            try
            {
                var result = string.Empty;
            JsonMediaTypeFormatter objFormatter = new JsonMediaTypeFormatter();
                response = EcosystemAPIHelper.GetEcosystemDeploymentHosts(siteId);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result = response.Content.ReadAsStringAsync().Result;
                    response.Content = new ObjectContent(result.GetType(), result, objFormatter);
                }
            }
            catch (Exception ex)
            {
                response = new HttpResponseMessage
            {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(ex.Message)
            };           
        }    
            return response;
        }

     }
}