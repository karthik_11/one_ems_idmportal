﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.DataObjects;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class OneEMSLinkController : ApiController
    {
        OneEMSDBHelper helper;
        ResponseObject responseObject;

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemFiltersController"/> class.
        /// </summary>
        public OneEMSLinkController()
        {
            helper = new OneEMSDBHelper();
        }

        public HttpResponseMessage Post([FromBody]ResponseObject respObj)
        {
            responseObject = new ResponseObject();
            try
            {
                var caseObjColl = JsonConvert.DeserializeObject<List<OneEMSCase>>(respObj.payload);
                foreach (var objCase in caseObjColl)
                {
                    if (objCase != null
                    && !string.IsNullOrEmpty(objCase.siteid)
                    && !string.IsNullOrEmpty(objCase.hostname)
                    && !string.IsNullOrEmpty(objCase.service)
                    && !string.IsNullOrEmpty(objCase.status)
                    && !string.IsNullOrEmpty(objCase.casenum))
                    {
                        objCase.casedate = DateTime.UtcNow;
                        objCase.oneemsstatus = "Open";
                        helper.InsertOneEMSCase(objCase);
                    }
                }

                responseObject.status = 1;
                responseObject.message = "success";
                responseObject.payload = string.Empty;
            }
            catch (Exception ex)
            {
                responseObject.status = 0;
                responseObject.message = ex.Message;
                responseObject.payload = string.Empty;
            }

            return new HttpResponseMessage()
            {
                Content = new JsonContent(JObject.Parse(JsonConvert.SerializeObject(responseObject)))
            };
        }
    }
}