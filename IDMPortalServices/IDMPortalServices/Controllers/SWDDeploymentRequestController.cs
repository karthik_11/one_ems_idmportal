﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.Logger;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using Newtonsoft.Json;
using Philips.HealthCare.IDM.Portal.IDM_ESBInterface;
using MongoDB.Bson;
using System.Net;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class SWDDeploymentRequestController : ApiController
    {
        SWDeploymentHelper helper;
        ResponseObject responseObject;

        /// <summary>
        /// Initializes a new instance of the SWDDeploymentRequestController class.
        /// </summary>
        public SWDDeploymentRequestController()
        {
            helper = new SWDeploymentHelper();
        }

        /// <summary>
        /// Get Method to request the Software Deployment Packages
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Get()
        {
            LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method of SWDeploymentController" });
            var json = JArray.Parse(helper.GetSWDeploymentGridData());
            return new HttpResponseMessage()
            {
                Content = new JsonContent(json)
            };
        }

        /// <summary>
        /// Creates a new SWDeployment message
        /// </summary>
        /// <param name="deploymentRequest"></param>
        /// <returns>all the messages based upon the criteria</returns>
        public HttpResponseMessage Post([FromBody] JsonStringArray jsonArray)
        {
            HttpResponseMessage response;
            responseObject = new ResponseObject();

            try
            {
                SWDeployment deploymentRequest = JsonConvert.DeserializeObject<SWDeployment>(jsonArray.array[0]);
                if (deploymentRequest == null)
                {
                    throw new Exception("Invalid Software Deployment Request.");
                }
                //var postSWDJson = new SWDJsonPost() { sites = swdRequest.sites, subscriptions = new List<string>() { swdRequest.name } };
                response = EcosystemAPIHelper.PostSWDeploymentRequest(jsonArray.array[1]);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    helper.SaveSWDeploymentRequest(deploymentRequest);
                    responseObject.status = 1;
                    responseObject.message = "Software Deployment request submitted successfully.";
                    responseObject.payload = JsonConvert.SerializeObject(deploymentRequest);
                }
                else
                {
                    responseObject.status = 0;
                    responseObject.message = "Error in submitting Software Deployment request to the IDM Ecosystem.";
                    responseObject.payload = "Success";
                }
            }
            catch (Exception ex)
            {
                responseObject.status = 0;
                responseObject.message = ex.Message;
                responseObject.payload = string.Empty;
            }

            return new HttpResponseMessage()
            {
                Content = new JsonContent(JObject.Parse(JsonConvert.SerializeObject(responseObject)))
            };
        }
    }
}