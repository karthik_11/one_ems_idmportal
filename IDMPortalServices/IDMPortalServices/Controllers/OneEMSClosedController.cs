﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.Logger;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class OneEMSClosedController : ApiController
    {
        OneEMSDBHelper helper;
        ResponseObject responseObject;

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemFiltersController"/> class.
        /// </summary>
        public OneEMSClosedController()
        {
            helper = new OneEMSDBHelper();
        }

        [HttpGet]
        [ActionName("OpenCases")]
        public HttpResponseMessage OpenCases()
        {
            responseObject = new ResponseObject();
            try
            {
                LogManager.LogWriter.WriteLog(new LogMessage() { Description = "Get all open cases from OneEMS Collection" });
                
                responseObject.status = 1;
                responseObject.message = "success";
                responseObject.payload = helper.GetAllOpenCases();
            }
            catch (Exception ex)
            {
                responseObject.status = 0;
                responseObject.message = ex.Message;
                responseObject.payload = string.Empty;
            }

            return new HttpResponseMessage()
            {
                Content = new JsonContent(JObject.Parse(JsonConvert.SerializeObject(responseObject)))
            };
        }

        [HttpPost]
        [ActionName("ClosedCases")]
        public HttpResponseMessage ClosedCases([FromBody] ResponseObject responseObj)
        {
            this.responseObject = new ResponseObject();

            try
            {
                var closedCaseColl = JsonConvert.DeserializeObject<List<OneEMSTicket>>(responseObj.payload);
                helper.UpdateClosedCases(closedCaseColl);
                responseObject.status = 1;
                responseObject.message = "success";
                responseObject.payload = string.Empty;
            }
            catch (Exception ex)
            {                
                responseObject.status = 0;
                responseObject.message = ex.Message;
                responseObject.payload = string.Empty;
            }

            return new HttpResponseMessage()
            {
                Content = new JsonContent(JObject.Parse(JsonConvert.SerializeObject(responseObject)))
            };
        }
    }
}