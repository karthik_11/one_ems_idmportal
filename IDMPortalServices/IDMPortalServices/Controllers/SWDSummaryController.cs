﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Newtonsoft.Json;
using MongoDB.Driver;
using System.Net.Http.Formatting;
using Philips.HealthCare.IDM.Portal.Logger;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers {
    public class SWDSummaryController : ApiController {
       
         MongoDbHelper helper; 
        /// <summary>
         /// Initializes a new instance of the <see cref="SWDSummaryController"/> class.
        /// </summary>
         public SWDSummaryController()
         {
             helper = new MongoDbHelper();
        }


         /// <summary>
         /// Post method of Software Distribution Controller
         /// </summary>
         /// <param name="criteria"></param>
         /// <returns>All the messages based upon the criteria</returns>
         public HttpResponseMessage Post([FromBody] PostJSONPayload payload)
         {

             JsonMediaTypeFormatter objFormatter = new JsonMediaTypeFormatter();
             string criteria = payload.payload;
             var settings = new JsonSerializerSettings() { DateFormatHandling = DateFormatHandling.MicrosoftDateFormat };
             var filter = JsonConvert.DeserializeObject<SearchCriteria>(criteria, settings);
             LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method of SWDListController" });
             MongoCursor<SWDRequestMsg> result = helper.SWDistributionSearchSummary(filter);
             var httpResponseMessage = new HttpResponseMessage()
             {
                 Content = new ObjectContent(result.GetType(), result, objFormatter)
             };
             return httpResponseMessage;
         }
        
    }
}