﻿using RabbitMQ.Client;
using System.Text;
using System.Configuration;

namespace Philips.HealthCare.IDM.Portal.IDM_ESBInterface
{
    public class SWDRequestMQProducer
    {
        private ConnectionFactory factory = new ConnectionFactory();

        public void SaveSWDRequestMQ(string message)
        {
            try
            {
                factory.HostName = ConfigurationManager.AppSettings["MQHostName"];

                using (IConnection connection = factory.CreateConnection())
                {
                    using (IModel channel = connection.CreateModel())
                    {
                        channel.ExchangeDeclare(ConfigurationManager.AppSettings["MQExchangeName"],
                            ConfigurationManager.AppSettings["MQQueueType"], true);
                        channel.QueueDeclare(ConfigurationManager.AppSettings["MQQueueName"], true, false, false, null);
                        channel.QueueBind(ConfigurationManager.AppSettings["MQQueueName"],
                            ConfigurationManager.AppSettings["MQExchangeName"], "");

                        IBasicProperties basicProperties = channel.CreateBasicProperties();
                        basicProperties.SetPersistent(true);
                        basicProperties.DeliveryMode = 2;

                        channel.TxSelect();
                        channel.BasicPublish(ConfigurationManager.AppSettings["MQExchangeName"],
                            "",
                            basicProperties,
                            Encoding.UTF8.GetBytes(message));
                        channel.TxCommit();
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}
