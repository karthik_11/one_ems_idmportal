﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips.HealthCare.IDM.Portal.DataAccess
{
    public static class HBDBHelper
    {
        public static DataSet GetPossibleSiteMatch(string siteKey, string matchType)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["HBDBConnection"]))
            {
                conn.Open();
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    string cmdText = string.Empty;
                    switch (matchType.ToLower())
                    {
                        case "equals":
                            cmdText = "SELECT SIT_KEY, SIT_NAME, SIT_VERSION, RegionName, dbo.UFN_SiteStatus(SIT_NAME) as SiteStatus FROM [HEARTBEAT].[Heartbeat].[dbo].[Sites] Sites " +
                                " Left Join [HEARTBEAT].[Heartbeat].[dbo].[SiteRegions] Regions ON Sites.SIT_REGION = Regions.Id " +  
                                " WHERE SIT_KEY='" + siteKey + "'";
                            break;
                        case "contains":
                            cmdText = "SELECT SIT_KEY, SIT_NAME, SIT_VERSION, RegionName, dbo.UFN_SiteStatus(SIT_NAME) as SiteStatus FROM [HEARTBEAT].[Heartbeat].[dbo].[Sites] Sites " +
                                " Left Join [HEARTBEAT].[Heartbeat].[dbo].[SiteRegions] Regions ON Sites.SIT_REGION = Regions.Id " +
                                " WHERE SIT_KEY like '%" + siteKey + "%'";
                            break;
                        default:
                            return null;
                    }
                    SqlCommand cmd = new SqlCommand(cmdText, conn);
                    adapter.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    return ds;
                }
            }
        }
    }
}
