﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Bson.Serialization;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.Utilities;
using System.Collections.ObjectModel;
using log4net;
using System.Configuration;


namespace Philips.HealthCare.IDM.Portal.DataAccess
{
    public class OneEMSDBHelper
    {
        private static ILog Logger = LogManager.GetLogger("IDM.Portal.DataAccess");
        private static string databaseName = ConfigurationManager.AppSettings["MongoDbDatabaseName"];
        private static string oneEMSCollectionName = ConfigurationManager.AppSettings["MongoDbOneEMSCollectionName"];
        private static string oneEMSClosedAuditCollectionName = ConfigurationManager.AppSettings["MongoDbOneEMSClosedAuditCollectionName"];

        public bool InsertOneEMSCase(OneEMSCase objCase)
        {
            try
            {
                var client = Utilities.GetConnection();
                //if (!client.GetServer().DatabaseExists(LogDatabasename)) {}
                var mongoDb = client.GetServer().GetDatabase(databaseName);
                //if (!mongoDb.CollectionExists(oneEMSCollectionName))
                //{
                    //Making a capped collection of 100MB                    
                //    mongoDb.CreateCollection(oneEMSCollectionName);
                //}
                var swdCollection = mongoDb.GetCollection(oneEMSCollectionName);
                var options = new MongoInsertOptions() { CheckElementNames = true, Flags = InsertFlags.ContinueOnError, SafeMode = SafeMode.True };
                swdCollection.Insert(objCase, options);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteOneEMSCase(OneEMSCase objCase)
        {
            try
            {
                var client = Utilities.GetConnection();
                //if (!client.GetServer().DatabaseExists(LogDatabasename)) {}
                var mongoDb = client.GetServer().GetDatabase(databaseName);
                var oneEMSCollection = mongoDb.GetCollection(oneEMSCollectionName);
                oneEMSCollection.Remove(Query.EQ("events_id", objCase.events_id));
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetAllOpenCases()
        {
            try
            {
                var oneEMSCollection = Utilities.GetSpecificCollection(databaseName, oneEMSCollectionName);
                var openCasesColl = oneEMSCollection.Find(Query.EQ("oneemsstatus", "Open"))
                    .SetFields(Fields.Include("case_number").Exclude("_id"));

                StringBuilder openCases = new StringBuilder();
                foreach (var item in openCasesColl)
                {
                    openCases.Append(item.GetElement("case_number").Value + ",");
                }
                return openCases.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateClosedCases(List<OneEMSTicket> closedCasesColl)
        {
            try
            {
                StringBuilder closedCases = new StringBuilder();
                var client = Utilities.GetConnection();
                var mongoDb = client.GetServer().GetDatabase(databaseName);

                if (!mongoDb.CollectionExists(oneEMSClosedAuditCollectionName))
                {
                    mongoDb.CreateCollection(oneEMSClosedAuditCollectionName);
                }

                var oneEMSAuditCollection = mongoDb.GetCollection(oneEMSClosedAuditCollectionName);

                foreach (var item in closedCasesColl)
                {
                    oneEMSAuditCollection.Insert(item);
                    closedCases.Append(item.casenumber + ",");
                }

                closedCases = closedCases.Remove(closedCases.Length - 1, 1);

                var oneEMSCollection = Utilities.GetSpecificCollection(databaseName, oneEMSCollectionName);
                var query = Query.In("case_number", new BsonArray(closedCases.ToString().Split(',')));
                var update = Update.Set("oneemsstatus", "Closed");
                oneEMSCollection.Update(query, update, UpdateFlags.Multi);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
